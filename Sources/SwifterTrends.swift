//
//  SwifterTrends.swift
//  Swifter
//
//  Generated on 2018-03-06. DO NOT EDIT.
//  Copyright © 2018 Matt Donnelly. All rights reserved.
//

extension Swifter {

    /**
    GET trends/place

    Returns the top 10 trending topics for a specific WOEID, if trending information is available for it.

    The response is an array of "trend" objects that encode the name of the trending topic, the query parameter that can be used to search for the topic on Twitter Search, and the Twitter Search URL.

    This information is cached for 5 minutes. Requesting more frequently than that will not return any more data, and will count against your rate limit usage.
    */
    public func getTrendsPlace(with woeid: String, excludeHashtags: Bool = false) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["id"] ??= woeid
        parameters["exclude"] ??= excludeHashtags ? "hashtags" : nil

        return self.getJSON(path: "trends/place.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    GET trends/available

    Returns the locations that Twitter has trending topic information for.

    The response is an array of "locations" that encode the location's WOEID and some other human-readable information such as a canonical name and country the location belongs in.

    A WOEID is a Yahoo! Where On Earth ID.
    */
    public func getAvailableTrends() -> DataPromise {
        return self.getJSON(path: "trends/available.json", baseURL: .api, parameters: [:])
            .map { data, _ in data }
    }

    /**
    GET trends/closest

    Returns the locations that Twitter has trending topic information for, closest to a specified location.

    The response is an array of "locations" that encode the location's WOEID and some other human-readable information such as a canonical name and country the location belongs in.

    A WOEID is a Yahoo! Where On Earth ID.
    */
    public func getClosestTrends(for coordinate: (lat: Double, long: Double)) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["long"] ??= coordinate.long
        parameters["lat"] ??= coordinate.lat

        return self.getJSON(path: "trends/closest.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

}
