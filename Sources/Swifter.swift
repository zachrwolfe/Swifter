//
//  Swifter.swift
//  Swifter
//
//  Copyright (c) 2014 Matt Donnelly.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

import Foundation
import Dispatch

#if os(macOS) || os(iOS)
import Accounts
#endif

extension Notification.Name {
    static let SwifterCallbackNotification: Notification.Name = Notification.Name(rawValue: "SwifterCallbackNotificationName")
}

// MARK: - Twitter URL
public enum TwitterURL {
    case api
    case upload
    case stream
    case userStream
    case siteStream
    case oauth
    
    var url: URL {
        switch self {
        case .api:          return URL(string: "https://api.twitter.com/1.1/")!
        case .upload:       return URL(string: "https://upload.twitter.com/1.1/")!
        case .stream:       return URL(string: "https://stream.twitter.com/1.1/")!
        case .userStream:   return URL(string: "https://userstream.twitter.com/1.1/")!
        case .siteStream:   return URL(string: "https://sitestream.twitter.com/1.1/")!
        case .oauth:        return URL(string: "https://api.twitter.com/")!
        }
    }
    
}

// MARK: - Tweet Mode
public enum TweetMode {
    case `default`
    case extended
    case compat
    case other(String)
    
    var stringValue: String? {
        switch self {
        case .default:
            return nil
        case .extended:
            return "extended"
        case .compat:
            return "compat"
        case .other(let string):
            return string
        }
    }
}

public class Swifter {
    
    // MARK: - Types

    public typealias DataPromise = Promise<Data>
    public typealias RawDataPromise = Promise<(Data, HTTPURLResponse)>
    public typealias TokenPromise = Promise<(Credential.OAuthAccessToken?, URLResponse)>

    internal struct CallbackNotification {
        static let optionsURLKey = "SwifterCallbackNotificationOptionsURLKey"
    }
    
    internal struct DataParameters {
        static let dataKey = "SwifterDataParameterKey"
        static let fileNameKey = "SwifterDataParameterFilename"
    }

    internal enum RequestMode {
        case rest, streaming
    }

    // MARK: - Static Properties
    public static let decoder: JSONDecoder = {
        let decoder = JSONDecoder()

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "eee MMM dd HH:mm:ss ZZZZ yyyy"
        dateFormatter.locale = Locale(identifier: "en_US")

        decoder.dateDecodingStrategy = .formatted(dateFormatter)

        return decoder
    }()

    // MARK: - Properties
    
    public var client: SwifterClientProtocol

    // MARK: - Initializers
    
    public init(consumerKey: String, consumerSecret: String, appOnly: Bool = false) {
        self.client = appOnly
            ? AppOnlyClient(consumerKey: consumerKey, consumerSecret: consumerSecret)
            : OAuthClient(consumerKey: consumerKey, consumerSecret: consumerSecret)
    }

    public init(consumerKey: String, consumerSecret: String, oauthToken: String, oauthTokenSecret: String) {
        self.client = OAuthClient(consumerKey: consumerKey, consumerSecret: consumerSecret , accessToken: oauthToken, accessTokenSecret: oauthTokenSecret)
    }

    #if os(macOS) || os(iOS)
    public init(account: ACAccount) {
        self.client = AccountsClient(account: account)
    }
    #endif

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    // MARK: - JSON Requests
    
    @discardableResult
    internal func jsonRequest(path: String, baseURL: TwitterURL, method: HTTPMethodType, parameters: Dictionary<String, Any>, uploadProgress: HTTPRequest.UploadProgressHandler? = nil, requestMode: RequestMode = .rest) -> RawDataPromise {
        let promise: RawPromise<(Data, HTTPURLResponse)>
        let jsonDownloadProgressHandler: HTTPRequest.DownloadProgressHandler?
        if requestMode == .streaming {
            let streamPromise = RawStreamPromise<(Data, HTTPURLResponse)>()
            promise = streamPromise
            jsonDownloadProgressHandler = { data, _, _, response in
                let chunkSeparator = "\r\n"
                if var jsonString = String(data: data, encoding: .utf8) {
                    if let remaining = streamPromise.chunkBuffer {
                        jsonString = remaining + jsonString
                    }
                    let jsonChunks = jsonString.components(separatedBy: chunkSeparator)
                    for chunk in jsonChunks where !chunk.utf16.isEmpty {
                        if let chunkData = chunk.data(using: .utf8) {
                            guard (try? JSON.parse(jsonData: chunkData)) != nil else {
                                streamPromise.chunkBuffer = chunk
                                return
                            }
                            streamPromise.chunkBuffer = nil
                            promise.succeed(with: (chunkData, response))
                        }
                    }
                }
            }
        } else {
            promise = RawPromise<(Data, HTTPURLResponse)>()
            jsonDownloadProgressHandler = nil
        }

        let jsonSuccessHandler: HTTPRequest.SuccessHandler = { data, response in
            if case 200 ... 299 = response.statusCode, data.count == 0 {
                promise.succeed(with: ("{}".data(using: .utf8)!, response))
            } else {
                promise.succeed(with: (data, response))
            }
        }

        if method == .GET {
            promise.request = self.client.get(path, baseURL: baseURL, parameters: parameters, uploadProgress: uploadProgress, downloadProgress: jsonDownloadProgressHandler, success: jsonSuccessHandler, failure: promise.fail(with:))
        } else {
            promise.request = self.client.post(path, baseURL: baseURL, parameters: parameters, uploadProgress: uploadProgress, downloadProgress: jsonDownloadProgressHandler, success: jsonSuccessHandler, failure: promise.fail(with:))
        }
        return promise
    }

    @discardableResult
    internal func getJSON(path: String, baseURL: TwitterURL, parameters: Dictionary<String, Any>, uploadProgress: HTTPRequest.UploadProgressHandler? = nil, requestMode: RequestMode = .rest) -> RawDataPromise {
        return self.jsonRequest(path: path, baseURL: baseURL, method: .GET, parameters: parameters, uploadProgress: uploadProgress, requestMode: requestMode)
    }

    @discardableResult
    internal func postJSON(path: String, baseURL: TwitterURL, parameters: Dictionary<String, Any>, uploadProgress: HTTPRequest.UploadProgressHandler? = nil, requestMode: RequestMode = .rest) -> RawDataPromise {
        return self.jsonRequest(path: path, baseURL: baseURL, method: .POST, parameters: parameters, uploadProgress: uploadProgress, requestMode: requestMode)
    }
    
}
