//
//  SwifterFollowers.swift
//  Swifter
//
//  Generated on 2018-03-06. DO NOT EDIT.
//  Copyright © 2018 Matt Donnelly. All rights reserved.
//

extension Swifter {

    /**
    GET friendships/no_retweets/ids

    Returns a collection of user_ids that the currently authenticated user does not want to receive retweets from. Use POST friendships/update to set the "no retweets" status for a given user account on behalf of the current user.
    */
    public func listOfNoRetweetsFriends(stringifyIDs: Bool = true) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["stringify_ids"] ??= stringifyIDs

        return self.getJSON(path: "friendships/no_retweets/ids.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    GET friends/ids

    Returns Users (*: user IDs for followees)

    Returns a cursored collection of user IDs for every user the specified user is following (otherwise known as their "friends").

    At this time, results are ordered with the most recent following first — however, this ordering is subject to unannounced change and eventual consistency issues. Results are given in groups of 5,000 user IDs and multiple "pages" of results can be navigated through using the next_cursor value in subsequent requests. See Using cursors to navigate collections for more information.

    This method is especially powerful when used in conjunction with GET users/lookup, a method that allows you to convert user IDs into full user objects in bulk.
    */
    public func getUserFollowingIDs(for userTag: UserTag, cursor: String? = nil, stringifyIDs: Bool? = nil, count: Int? = nil) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["count"] ??= count
        parameters[userTag.key] ??= userTag.value
        parameters["cursor"] ??= cursor
        parameters["stringify_ids"] ??= stringifyIDs

        return self.getJSON(path: "friends/ids.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    GET friendships/incoming

    Returns a collection of numeric IDs for every user who has a pending request to follow the authenticating user.
    */
    public func getIncomingPendingFollowRequests(cursor: String? = nil, stringifyIDs: String? = nil) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["cursor"] ??= cursor
        parameters["stringify_ids"] ??= stringifyIDs

        return self.getJSON(path: "friendships/incoming.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    GET friendships/outgoing

    Returns a collection of numeric IDs for every protected user for whom the authenticating user has a pending follow request.
    */
    public func getOutgoingPendingFollowRequests(cursor: String? = nil, stringifyIDs: String? = nil) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["cursor"] ??= cursor
        parameters["stringify_ids"] ??= stringifyIDs

        return self.getJSON(path: "friendships/outgoing.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    POST friendships/create

    Allows the authenticating users to follow the user specified in the ID parameter.

    Returns the befriended user in the requested format when successful. Returns a string describing the failure condition when unsuccessful. If you are already friends with the user a HTTP 403 may be returned, though for performance reasons you may get a 200 OK message even if the friendship already exists.

    Actions taken in this method are asynchronous and changes will be eventually consistent.
    */
    public func followUser(for userTag: UserTag, follow: Bool? = nil) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters[userTag.key] ??= userTag.value
        parameters["follow"] ??= follow

        return self.postJSON(path: "friendships/create.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    POST friendships/destroy

    Allows the authenticating user to unfollow the user specified in the ID parameter.

    Returns the unfollowed user in the requested format when successful. Returns a string describing the failure condition when unsuccessful.
    */
    public func unfollowUser(for userTag: UserTag) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters[userTag.key] ??= userTag.value

        return self.postJSON(path: "friendships/destroy.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    POST friendships/update

    Allows one to enable or disable retweets and device notifications from the specified user.
    */
    public func updateFriendship(with userTag: UserTag, device: Bool? = nil, retweets: Bool? = nil) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["device"] ??= device
        parameters[userTag.key] ??= userTag.value
        parameters["retweets"] ??= retweets

        return self.postJSON(path: "friendships/update.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    GET friendships/show

    Returns detailed information about the relationship between two arbitrary users.
    */
    public func showFriendship(between sourceTag: UserTag, and targetTag: UserTag) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["source_\(sourceTag.key)"] ??= sourceTag.value
        parameters["target_\(targetTag.key)"] ??= targetTag.value

        return self.getJSON(path: "friendships/show.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    GET friends/list

    Returns a cursored collection of user objects for every user the specified user is following (otherwise known as their "friends").

    At this time, results are ordered with the most recent following first — however, this ordering is subject to unannounced change and eventual consistency issues. Results are given in groups of 20 users and multiple "pages" of results can be navigated through using the next_cursor value in subsequent requests. See Using cursors to navigate collections for more information.
    */
    public func getUserFollowing(for userTag: UserTag, cursor: String? = nil, count: Int? = nil, skipStatus: Bool? = nil, includeUserEntities: Bool? = nil) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["include_user_entities"] ??= includeUserEntities
        parameters["count"] ??= count
        parameters["cursor"] ??= cursor
        parameters["user_tag"] ??= userTag.value
        parameters["skip_status"] ??= skipStatus

        return self.getJSON(path: "friends/list.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    GET followers/list

    Returns a cursored collection of user objects for users following the specified user.

    At this time, results are ordered with the most recent following first — however, this ordering is subject to unannounced change and eventual consistency issues. Results are given in groups of 20 users and multiple "pages" of results can be navigated through using the next_cursor value in subsequent requests. See Using cursors to navigate collections for more information.
    */
    public func getUserFollowers(for userTag: UserTag, cursor: String? = nil, count: Int? = nil, skipStatus: Bool? = nil, includeUserEntities: Bool? = nil) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["include_user_entities"] ??= includeUserEntities
        parameters["count"] ??= count
        parameters["cursor"] ??= cursor
        parameters["user_tag"] ??= userTag.value
        parameters["skip_status"] ??= skipStatus

        return self.getJSON(path: "followers/list.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    GET friendships/lookup

    Returns the relationships of the authenticating user to the comma-separated list of up to 100 screen_names or user_ids provided. Values for connections can be: following, following_requested, followed_by, none.
    */
    public func lookupFriendship(with usersTag: UsersTag) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters[usersTag.key] ??= usersTag.value

        return self.getJSON(path: "friendships/lookup.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

}
