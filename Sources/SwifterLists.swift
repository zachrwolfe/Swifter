//
//  SwifterLists.swift
//  Swifter
//
//  Generated on 2018-03-06. DO NOT EDIT.
//  Copyright © 2018 Matt Donnelly. All rights reserved.
//

extension Swifter {

    /**
    GET lists/list

    Returns all lists the authenticating or specified user subscribes to, including their own. The user is specified using the user_id or screen_name parameters. If no user is given, the authenticating user is used.

    This method used to be GET lists in version 1.0 of the API and has been renamed for consistency with other call.

    A maximum of 100 results will be returned by this call. Subscribed lists are returned first, followed by owned lists. This means that if a user subscribes to 90 lists and owns 20 lists, this method returns 90 subscriptions and 10 owned lists. The reverse method returns owned lists first, so with reverse=true, 20 owned lists and 80 subscriptions would be returned. If your goal is to obtain every list a user owns or subscribes to, use GET lists/ownerships and/or GET lists/subscriptions instead.
    */
    public func getSubscribedLists(for userTag: UserTag? = nil, reverse: Bool? = nil) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters[userTag?.key] ??= userTag?.value
        parameters["reverse"] ??= reverse

        return self.getJSON(path: "lists/list.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    GET lists/statuses

    Returns a timeline of tweets authored by members of the specified list. Retweets are included by default. Use the include_rts=false parameter to omit retweets. Embedded Timelines is a great way to embed list timelines on your website.
    */
    public func listTweets(for listTag: ListTag, sinceID: UInt64?, maxID: UInt64?, count: Int?, includeEntities: Bool?, includeRTs: Bool?, tweetMode: TweetMode = TweetMode.default) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["since_id"] ??= sinceID
        parameters["include_rts"] ??= includeRTs
        parameters[listTag.ownerKey] ??= listTag.ownerValue
        parameters["count"] ??= count
        parameters["tweet_mode"] ??= tweetMode.stringValue
        parameters[listTag.key] ??= listTag.value
        parameters["max_id"] ??= maxID
        parameters["include_entities"] ??= includeEntities

        return self.getJSON(path: "lists/statuses.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    POST lists/members/destroy

    Removes the specified member from the list. The authenticated user must be the list's owner to remove members from the list.
    */
    public func removeMemberFromList(for listTag: ListTag, user userTag: UserTag) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters[listTag.key] ??= listTag.value
        parameters[listTag.ownerKey] ??= listTag.ownerValue
        parameters[userTag.key] ??= userTag.value

        return self.postJSON(path: "lists/members/destroy.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    GET lists/memberships

    Returns the lists the specified user has been added to. If user_id or screen_name are not provided the memberships for the authenticating user are returned.
    */
    public func getListMemberships(for userTag: UserTag, count: Int? = nil, cursor: String?, filterToOwnedLists: Bool?) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["count"] ??= count
        parameters[userTag.key] ??= userTag.value
        parameters["cursor"] ??= cursor
        parameters["filter_to_owned_lists"] ??= filterToOwnedLists

        return self.getJSON(path: "lists/memberships.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    GET lists/subscribers

    Returns the subscribers of the specified list. Private list subscribers will only be shown if the authenticated user owns the specified list.
    */
    public func getListSubscribers(for listTag: ListTag, cursor: String?, includeEntities: Bool?, skipStatus: Bool?) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["skip_status"] ??= skipStatus
        parameters[listTag.ownerKey] ??= listTag.ownerValue
        parameters[listTag.key] ??= listTag.value
        parameters["cursor"] ??= cursor
        parameters["include_entities"] ??= includeEntities

        return self.getJSON(path: "lists/subscribers.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    POST lists/subscribers/create

    Subscribes the authenticated user to the specified list.
    */
    public func subscribeToList(for listTag: ListTag) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters[listTag.key] ??= listTag.value
        parameters[listTag.ownerKey] ??= listTag.ownerValue

        return self.postJSON(path: "lists/subscribers/create.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    GET lists/subscribers/show

    Check if the specified user is a subscriber of the specified list. Returns the user if they are subscriber.
    */
    public func checkListSubcription(of userTag: UserTag, for listTag: ListTag, includeEntities: Bool?, skipStatus: Bool?) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["skip_status"] ??= skipStatus
        parameters[listTag.ownerKey] ??= listTag.ownerValue
        parameters[listTag.key] ??= listTag.value
        parameters[userTag.key] ??= userTag.value
        parameters["include_entities"] ??= includeEntities

        return self.getJSON(path: "lists/subscribers/show.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    POST lists/subscribers/destroy

    Unsubscribes the authenticated user from the specified list.
    */
    public func unsubscribeFromList(for listTag: ListTag) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters[listTag.key] ??= listTag.value
        parameters[listTag.ownerKey] ??= listTag.ownerValue

        return self.postJSON(path: "lists/subscribers/destroy.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    POST lists/members/create_all

    Adds multiple members to a list, by specifying a comma-separated list of member ids or screen names. The authenticated user must own the list to be able to add members to it. Note that lists can't have more than 5,000 members, and you are limited to adding up to 100 members to a list at a time with this method.

    Please note that there can be issues with lists that rapidly remove and add memberships. Take care when using these methods such that you are not too rapidly switching between removals and adds on the same list.
    */
    public func subscribeUsersToList(for listTag: ListTag, users usersTag: UsersTag, includeEntities: Bool?, skipStatus: Bool?) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["skip_status"] ??= skipStatus
        parameters[listTag.ownerKey] ??= listTag.ownerValue
        parameters[listTag.key] ??= listTag.value
        parameters[usersTag.key] ??= usersTag.value
        parameters["include_entities"] ??= includeEntities

        return self.postJSON(path: "lists/members/create_all.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    GET lists/members/show

    Check if the specified user is a member of the specified list.
    */
    public func checkListMembership(of userTag: UserTag, for listTag: ListTag, includeEntities: Bool?, skipStatus: Bool?) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["skip_status"] ??= skipStatus
        parameters[listTag.ownerKey] ??= listTag.ownerValue
        parameters[listTag.key] ??= listTag.value
        parameters[userTag.key] ??= userTag.value
        parameters["include_entities"] ??= includeEntities

        return self.getJSON(path: "lists/members/show.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    GET lists/members

    Returns the members of the specified list. Private list members will only be shown if the authenticated user owns the specified list.
    */
    public func getListMembers(for listTag: ListTag, cursor: String?, includeEntities: Bool?, skipStatus: Bool?) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["skip_status"] ??= skipStatus
        parameters[listTag.ownerKey] ??= listTag.ownerValue
        parameters[listTag.key] ??= listTag.value
        parameters["cursor"] ??= cursor
        parameters["include_entities"] ??= includeEntities

        return self.getJSON(path: "lists/members.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    POST lists/members/create

    Add a member to a list. The authenticated user must own the list to be able to add members to it. Note that lists cannot have more than 5,000 members.
    */
    public func addListMember(_ userTag: UserTag, for listTag: ListTag) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters[listTag.key] ??= listTag.value
        parameters[userTag.key] ??= userTag.value
        parameters[listTag.ownerKey] ??= listTag.ownerValue

        return self.postJSON(path: "lists/members/create.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    POST lists/destroy

    Deletes the specified list. The authenticated user must own the list to be able to destroy it.
    */
    public func deleteList(for listTag: ListTag) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters[listTag.key] ??= listTag.value
        parameters[listTag.ownerKey] ??= listTag.ownerValue

        return self.postJSON(path: "lists/destroy.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    POST lists/update

    Updates the specified list. The authenticated user must own the list to be able to update it.
    */
    public func updateList(for listTag: ListTag, name: String?, isPublic: Bool = true, description: String?) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters[listTag.ownerKey] ??= listTag.ownerValue
        parameters["name"] ??= name
        parameters[listTag.key] ??= listTag.value
        parameters["is_public"] ??= isPublic ? "public" : "private"
        parameters["description"] ??= description

        return self.postJSON(path: "lists/update.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    POST lists/create

    Creates a new list for the authenticated user. Note that you can't create more than 20 lists per account.
    */
    public func createList(named name: String, asPublicList: Bool = true, description: String?) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["description"] ??= description
        parameters["name"] ??= name
        parameters["as_public_list"] ??= asPublicList ? "public" : "private"

        return self.postJSON(path: "lists/create.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    GET lists/show

    Returns the specified list. Private lists will only be shown if the authenticated user owns the specified list.
    */
    public func showList(for listTag: ListTag) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters[listTag.key] ??= listTag.value
        parameters[listTag.ownerKey] ??= listTag.ownerValue

        return self.getJSON(path: "lists/show.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    GET lists/subscriptions

    Obtain a collection of the lists the specified user is subscribed to, 20 lists per page by default. Does not include the user's own lists.
    */
    public func getSubscribedList(of userTag: UserTag, count: String?, cursor: String?) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters[userTag.key] ??= userTag.value
        parameters["count"] ??= count
        parameters["cursor"] ??= cursor

        return self.getJSON(path: "lists/subscriptions.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    POST lists/members/destroy_all

    Removes multiple members from a list, by specifying a comma-separated list of member ids or screen names. The authenticated user must own the list to be able to remove members from it. Note that lists can't have more than 500 members, and you are limited to removing up to 100 members to a list at a time with this method.

    Please note that there can be issues with lists that rapidly remove and add memberships. Take care when using these methods such that you are not too rapidly switching between removals and adds on the same list.
    */
    public func removeListMembers(_ usersTag: UsersTag, for listTag: ListTag) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters[listTag.key] ??= listTag.value
        parameters[listTag.ownerKey] ??= listTag.ownerValue
        parameters[usersTag.key] ??= usersTag.value

        return self.postJSON(path: "lists/members/destroy_all.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    GET lists/ownerships

    Returns the lists owned by the specified Twitter user. Private lists will only be shown if the authenticated user is also the owner of the lists.
    */
    public func getOwnedLists(for userTag: UserTag, count: String?, cursor: String?) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters[userTag.key] ??= userTag.value
        parameters["count"] ??= count
        parameters["cursor"] ??= cursor

        return self.getJSON(path: "lists/ownerships.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

}
