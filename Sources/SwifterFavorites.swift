//
//  SwifterFavorites.swift
//  Swifter
//
//  Generated on 2018-03-06. DO NOT EDIT.
//  Copyright © 2018 Matt Donnelly. All rights reserved.
//

extension Swifter {

    /**
    GET favorites/list

    Returns the 20 most recent Tweets favorited by the authenticating or specified user.

    If you do not provide either a user_id or screen_name to this method, it will assume you are requesting on behalf of the authenticating user. Specify one or the other for best results.
    */
    public func getRecentlyFavoritedTweets(count: Int? = nil, sinceID: UInt64? = nil, maxID: UInt64? = nil, tweetMode: TweetMode = TweetMode.default) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["since_id"] ??= sinceID
        parameters["count"] ??= count
        parameters["tweet_mode"] ??= tweetMode.stringValue
        parameters["max_id"] ??= maxID

        return self.getJSON(path: "favorites/list.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    public func getRecentlyFavoritedTweets(for userTag: UserTag, count: Int? = nil, sinceID: UInt64? = nil, maxID: UInt64? = nil, tweetMode: TweetMode = TweetMode.default) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["since_id"] ??= sinceID
        parameters["count"] ??= count
        parameters["tweet_mode"] ??= tweetMode.stringValue
        parameters[userTag.key] ??= userTag.value
        parameters["max_id"] ??= maxID

        return self.getJSON(path: "favorites/list.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    POST favorites/destroy

    Un-favorites the status specified in the ID parameter as the authenticating user. Returns the un-favorited status in the requested format when successful.

    This process invoked by this method is asynchronous. The immediately returned status may not indicate the resultant favorited status of the tweet. A 200 OK response from this method will indicate whether the intended action was successful or not.
    */
    public func unfavoriteTweet(forID id: UInt64, includeEntities: Bool? = nil, tweetMode: TweetMode = TweetMode.default) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["id"] ??= id
        parameters["tweet_mode"] ??= tweetMode.stringValue
        parameters["include_entities"] ??= includeEntities

        return self.postJSON(path: "favorites/destroy.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    POST favorites/create

    Favorites the status specified in the ID parameter as the authenticating user. Returns the favorite status when successful.

    This process invoked by this method is asynchronous. The immediately returned status may not indicate the resultant favorited status of the tweet. A 200 OK response from this method will indicate whether the intended action was successful or not.
    */
    public func favoriteTweet(forID id: UInt64, includeEntities: Bool? = nil, tweetMode: TweetMode = TweetMode.default) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["id"] ??= id
        parameters["tweet_mode"] ??= tweetMode.stringValue
        parameters["include_entities"] ??= includeEntities

        return self.postJSON(path: "favorites/create.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

}
