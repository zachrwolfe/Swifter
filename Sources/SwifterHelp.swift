//
//  SwifterHelp.swift
//  Swifter
//
//  Generated on 2018-03-06. DO NOT EDIT.
//  Copyright © 2018 Matt Donnelly. All rights reserved.
//

extension Swifter {

    /**
    GET help/configuration

    Returns the current configuration used by Twitter including twitter.com slugs which are not usernames, maximum photo resolutions, and t.co URL lengths.

    It is recommended applications request this endpoint when they are loaded, but no more than once a day.
    */
    public func getHelpConfiguration() -> DataPromise {
        return self.getJSON(path: "help/configuration.json", baseURL: .api, parameters: [:])
            .map { data, _ in data }
    }

    /**
    GET help/languages

    Returns the list of languages supported by Twitter along with their ISO 639-1 code. The ISO 639-1 code is the two letter value to use if you include lang with any of your requests.
    */
    public func getHelpLanguages() -> DataPromise {
        return self.getJSON(path: "help/languages.json", baseURL: .api, parameters: [:])
            .map { data, _ in data }
    }

    /**
    GET help/privacy

    Returns Twitter's Privacy Policy.
    */
    public func getHelpPrivacy() -> DataPromise {
        return self.getJSON(path: "help/privacy.json", baseURL: .api, parameters: [:])
            .map { data, _ in data }
    }

    /**
    GET help/tos

    Returns the Twitter Terms of Service in the requested format. These are not the same as the Developer Rules of the Road.
    */
    public func getHelpTermsOfService() -> DataPromise {
        return self.getJSON(path: "help/tos.json", baseURL: .api, parameters: [:])
            .map { data, _ in data }
    }

    /**
    GET help/configuration

    Returns the current rate limits for methods belonging to the specified resource families.

    Each 1.1 API resource belongs to a "resource family" which is indicated in its method documentation. You can typically determine a method's resource family from the first component of the path after the resource version.

    This method responds with a map of methods belonging to the families specified by the resources parameter, the current remaining uses for each of those resources within the current rate limiting window, and its expiration time in epoch time. It also includes a rate_limit_context field that indicates the current access token or application-only authentication context.

    You may also issue requests to this method without any parameters to receive a map of all rate limited GET methods. If your application only uses a few of methods, please explicitly provide a resources parameter with the specified resource families you work with.

    When using app-only auth, this method's response indicates the app-only auth rate limiting context.

    Read more about REST API Rate Limiting in v1.1 and review the limits.
    */
    public func getRateLimits(for resources: [String]) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["resources"] ??= resources.joined(separator: ",")

        return self.getJSON(path: "help/configuration.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

}
