//
//  SwifterSearch.swift
//  Swifter
//
//  Generated on 2018-03-06. DO NOT EDIT.
//  Copyright © 2018 Matt Donnelly. All rights reserved.
//

extension Swifter {

    /**
    GET search/tweets

    Returns a collection of relevant Tweets matching a specified query.

    Please note that Twitter’s search service and, by extension, the Search API is not meant to be an exhaustive source of Tweets. Not all Tweets will be indexed or made available via the search interface.

    In API v1.1, the response format of the Search API has been improved to return Tweet objects more similar to the objects you’ll find across the REST API and platform. However, perspectival attributes (fields that pertain to the perspective of the authenticating user) are not currently supported on this endpoint.
    */
    public func searchTweet(using query: String, geocode: String? = nil, lang: String? = nil, locale: String? = nil, resultType: String? = nil, count: Int? = nil, until: String? = nil, sinceID: UInt64? = nil, maxID: UInt64? = nil, includeEntities: Bool? = nil, callback: String? = nil, tweetMode: TweetMode = TweetMode.default) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["include_entities"] ??= includeEntities
        parameters["lang"] ??= lang
        parameters["since_id"] ??= sinceID
        parameters["callback"] ??= callback
        parameters["q"] ??= query
        parameters["until"] ??= until
        parameters["result_type"] ??= resultType
        parameters["count"] ??= count
        parameters["tweet_mode"] ??= tweetMode.stringValue
        parameters["geocode"] ??= geocode
        parameters["locale"] ??= locale
        parameters["max_id"] ??= maxID

        return self.getJSON(path: "search/tweets.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

}
