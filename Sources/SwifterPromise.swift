//
//  SwifterPromise.swift
//  Swifter
//
//  Copyright (c) 2014 Matt Donnelly.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

import Foundation

public class Promise<Result> {
    public static func succeedingPromise(with result: Result) -> Promise<Result> {
        let promise = Promise<Result>()
        promise.succeed(with: result)
        return promise
    }

    public static func failingPromise(with error: Error) -> Promise<Result> {
        let promise = Promise<Result>()
        promise.fail(with: error)
        return promise
    }

    public func start() {
        isStarted = true
        for message in prestartQueue {
            sendMessage(message)
        }
        prestartQueue.removeAll(keepingCapacity: false)
    }

    public func cancel() {}

    public final func flatMap<T>(strongly: Bool = false, _ transform: @escaping (Result) throws -> T) -> Promise<T> {
        let subPromise: MappedPromise<T, Result>
        if strongly {
            subPromise = StrongMappedPromise<T, Result>(superPromise: self)
        } else {
            subPromise = MappedPromise<T, Result>(superPromise: self)
        }
        _ = self.then { result in
            subPromise.succeed(with: try transform(result))
        }.catch(subPromise.fail(with:))
        return subPromise
    }

    public final func map<T>(strongly: Bool = false, _ transform: @escaping (Result) -> T) -> Promise<T> {
        let subPromise: MappedPromise<T, Result>
        if strongly {
            subPromise = StrongMappedPromise<T, Result>(superPromise: self)
        } else {
            subPromise = MappedPromise<T, Result>(superPromise: self)
        }
        _ = self.then { result in
            subPromise.succeed(with: transform(result))
        }.catch(subPromise.fail(with:))
        return subPromise
    }

    public final func then(_ handler: @escaping (Result) throws -> Void) -> Promise<Result> {
        if isStarted {
            print("SWIFTER WARNING: .then() called on a Promise after it was started. This could lead to unexpected results")
        }
        successSubscribers.append(handler)
        return self
    }

    public final func `catch`(_ handler: @escaping (Error) -> Void) -> Promise<Result> {
        if isStarted {
            print("SWIFTER WARNING: .catch() called on a Promise after it was started. This could lead to unexpected results")
        }
        failureSubscribers.append(handler)
        return self
    }

    public final func openChain<T>(_ chainOpening: @escaping (Result, ChainedPromise<T, Result>) -> Void) -> Promise<T> {
        let chainedPromise = ChainedPromise<T, Result>(superPromise: self)
        _ = self.then { result in chainOpening(result, chainedPromise) }
            .catch(chainedPromise.fail(with:))
        return chainedPromise
    }

    public final func closeChain<T>(to chainedPromise: ChainedPromise<Result, T>) {
        self.then { result in
            chainedPromise.subPromise = self
            chainedPromise.succeed(with: result)
        }.catch(chainedPromise.fail(with:))
        .start()
    }

    public final func chain<T>(to chainPromise: @escaping (Result) -> Promise<T>?) -> Promise<T> {
        return chainPersistingData(to: chainPromise)
            .map { new, old in new }
    }

    public final func chainDiscardingData<T>(to chainPromise: @escaping (Result) -> Promise<T>?) -> Promise<Result> {
        return chainPersistingData(to: chainPromise)
            .map { new, old in old }
    }

    public final func chainPersistingData<T>(to chainPromise: @escaping (Result) -> Promise<T>?) -> Promise<(T, Result)> {
        let intermediaryChainPromise = ChainedPromise<(T, Result), Result>(superPromise: self)
        _ = self.then { previousResult in
            guard let chainPromise = chainPromise(previousResult)?
                .map(strongly: true, { result in (result, previousResult) })
                else { return }

            intermediaryChainPromise.subPromise = chainPromise
            chainPromise
                .then { result in intermediaryChainPromise.succeed(with: result) }
                .catch { error in intermediaryChainPromise.fail(with: error) }
                .start()
        }
            .catch { error in intermediaryChainPromise.fail(with: error) }
        return intermediaryChainPromise
    }

    private final func sendMessage(_ message: Message) {
        guard isStarted else {
            chained = true
            prestartQueue.append(message)
            return
        }

        switch message {
        case let .success(result):
            for subscriber in successSubscribers {
                do {
                    try subscriber(result)
                } catch {
                    sendMessage(.failure(error))
                    break
                }
            }
        case let .failure(error):
            for subscriber in failureSubscribers {
                subscriber(error)
            }
        }
    }

    internal final func succeed(with result: Result) {
        self.sendMessage(.success(result))
    }

    internal final func fail(with error: Error) {
        self.sendMessage(.failure(error))
    }

    fileprivate final var successSubscribers: [(Result) throws -> Void] = []
    fileprivate final var failureSubscribers: [(Error) -> Void] = []
    internal enum Message {
        case success(Result)
        case failure(Error)
    }
    private var prestartQueue: [Message] = []
    private var isStarted = false

    internal init() {}

    private var chained = false
}

public class RawPromise<Result>: Promise<Result> {
    public override func start() {
        super.start()
        request?.start()

        // prevent reference cycles
        strongRequest = nil
    }

    public override func cancel() {
        request?.stop()
    }

    internal weak var request: HTTPRequest? {
        didSet {
            strongRequest = request
        }
    }
    private var strongRequest: HTTPRequest?
}

public class MappedPromise<Result, PreMappedResult>: Promise<Result> {
    public override func start() {
        superPromise?.start()
        super.start()
    }
    public override func cancel() {
        superPromise?.cancel()
    }

    fileprivate init(superPromise: Promise<PreMappedResult>) {
        self.superPromise = superPromise
        super.init()
    }

    fileprivate weak var superPromise: Promise<PreMappedResult>?
}

public class StrongMappedPromise<Result, PreMappedResult>: MappedPromise<Result, PreMappedResult> {
    override weak var superPromise: Promise<PreMappedResult>? {
        get { return _superPromise }
        set { _superPromise = newValue! }
    }
    fileprivate override init(superPromise: Promise<PreMappedResult>) {
        self._superPromise = superPromise
        super.init(superPromise: superPromise)
    }
    private var _superPromise: Promise<PreMappedResult>
}

public class ChainedPromise<Result, PreMappedResult>: MappedPromise<Result, PreMappedResult> {
    public override func cancel() {
        subPromise?.cancel()
        super.cancel()
    }
    fileprivate var subPromise: Promise<Result>?
}

public class RawStreamPromise<Result>: RawPromise<Result> {
    internal var chunkBuffer: String?
}

public extension Promise where Result == Data {
    public func decode<T: Decodable>(_ type: T.Type) -> Promise<T> {
        return self.flatMap { data in try Swifter.decoder.decode(type, from: data) }
    }

    public func decodeJSON() -> Promise<JSON> {
        return self.flatMap { data in try JSON.parse(jsonData: data) }
    }
}

public extension Promise where Result == (Data, HTTPURLResponse) {
    public func decode<T: Decodable>(_ type: T.Type) -> Promise<(T, HTTPURLResponse)> {
        return self.flatMap { data, response in (try Swifter.decoder.decode(type, from: data), response) }
    }

    public func decodeJSON() -> Promise<(JSON, HTTPURLResponse)> {
        return self.flatMap { data, response in (try JSON.parse(jsonData: data), response) }
    }
}
