//
//  SwifterSavedSearches.swift
//  Swifter
//
//  Generated on 2018-03-06. DO NOT EDIT.
//  Copyright © 2018 Matt Donnelly. All rights reserved.
//

extension Swifter {

    /**
    GET saved_searches/list

    Returns the authenticated user's saved search queries.
    */
    public func getSavedSearchesList() -> DataPromise {
        return self.getJSON(path: "saved_searches/list.json", baseURL: .api, parameters: [:])
            .map { data, _ in data }
    }

    /**
    GET saved_searches/show/:id

    Retrieve the information for the saved search represented by the given id. The authenticating user must be the owner of saved search ID being requested.
    */
    public func showSavedSearch(for id: UInt64) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["id"] ??= id

        return self.getJSON(path: "saved_searches/show/\(id).json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    POST saved_searches/create

    Create a new saved search for the authenticated user. A user may only have 25 saved searches.
    */
    public func createSavedSearch(for query: String) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["query"] ??= query

        return self.postJSON(path: "saved_searches/create.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    POST saved_searches/destroy/\(id)

    Destroys a saved search for the authenticating user. The authenticating user must be the owner of saved search id being destroyed.
    */
    public func deleteSavedSearch(for id: UInt64) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["id"] ??= id

        return self.postJSON(path: "saved_searches/destroy/\(id).json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

}
