//
//  SwifterAuth.swift
//  Swifter
//
//  Copyright (c) 2014 Matt Donnelly.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

import Foundation

#if os(iOS)
    import UIKit
    import SafariServices
#elseif os(macOS)
    import AppKit
#endif

public extension Swifter {

    /**
     Begin Authorization with a Callback URL.
     - OS X only
     */
    #if os(macOS)
    public func authorize(with callbackURL: URL) -> TokenPromise {
        return self.postOAuthRequestToken(with: callbackURL)
            .openChain { result, accessTokenPromise in
                let (token, _) = result
                var requestToken = token!

                NotificationCenter.default.addObserver(forName: .SwifterCallbackNotification, object: nil, queue: .main) { notification in
                    NotificationCenter.default.removeObserver(self)
                    let url = notification.userInfo![CallbackNotification.optionsURLKey] as! URL
                    let parameters = url.query!.queryStringParameters
                    requestToken.verifier = parameters["oauth_verifier"]
                    self.postOAuthAccessToken(with: requestToken)
                        .then { accessToken, response in
                            self.client.credential = Credential(accessToken: accessToken!)
                        }.closeChain(to: accessTokenPromise)
                        .start()
                }

                let authorizeURL = URL(string: "oauth/authorize", relativeTo: TwitterURL.oauth.url)
                let queryURL = URL(string: authorizeURL!.absoluteString + "?oauth_token=\(token!.key)")!
                NSWorkspace.shared.open(queryURL)
            }
    }
    #endif
    
    /**
     Begin Authorization with a Callback URL
     
     - Parameter presentFromViewController: The viewController used to present the SFSafariViewController.
     The UIViewController must inherit SFSafariViewControllerDelegate
     
     */
    
    #if os(iOS)
    public func authorize(with callbackURL: URL, presentFrom presentingViewController: UIViewController?) -> TokenPromise {
        return self.postOAuthRequestToken(with: callbackURL)
            .openChain { result, accessTokenPromise in
                let (token, _) = result
                var requestToken = token!
                NotificationCenter.default.addObserver(forName: .SwifterCallbackNotification, object: nil, queue: .main) { notification in
                    NotificationCenter.default.removeObserver(self)
                    presentingViewController?.presentedViewController?.dismiss(animated: true, completion: nil)
                    let url = notification.userInfo![CallbackNotification.optionsURLKey] as! URL

                    let parameters = url.query!.queryStringParameters
                    requestToken.verifier = parameters["oauth_verifier"]

                    self.postOAuthAccessToken(with: requestToken)
                        .then { accessToken, response in
                            self.client.credential = Credential(accessToken: accessToken!)
                        }
                        .closeChain(to: accessTokenPromise)
                }

                let authorizeURL = URL(string: "oauth/authorize", relativeTo: TwitterURL.oauth.url)
                let queryURL = URL(string: authorizeURL!.absoluteString + "?oauth_token=\(token!.key)")!

                if #available(iOS 9.0, *) , let delegate = presentingViewController as? SFSafariViewControllerDelegate {
                    let safariView = SFSafariViewController(url: queryURL)
                    safariView.delegate = delegate
                    presentingViewController?.present(safariView, animated: true, completion: nil)
                } else {
                    UIApplication.shared.openURL(queryURL)
                }
            }
    }
    #endif
    
    public class func handleOpenURL(_ url: URL) {
        let notification = Notification(name: .SwifterCallbackNotification, object: nil, userInfo: [CallbackNotification.optionsURLKey: url])
        NotificationCenter.default.post(notification)
    }
    
    public func authorizeAppOnly() -> TokenPromise {
        return self.postOAuth2BearerToken()
            .decodeJSON()
            .flatMap { json, response in
                if let tokenType = json["token_type"].string {
                    if tokenType == "bearer" {
                        let accessToken = json["access_token"].string

                        let credentialToken = Credential.OAuthAccessToken(key: accessToken!, secret: "")

                        self.client.credential = Credential(accessToken: credentialToken)

                        return (credentialToken, response)
                    } else {
                        throw SwifterError(message: "Cannot find bearer token in server response", kind: .invalidAppOnlyBearerToken)
                    }
                } else if case .object = json["errors"] {
                    throw SwifterError(message: json["errors"]["message"].string!, kind: .responseError(code: json["errors"]["code"].integer!))
                } else {
                    throw SwifterError(message: "Cannot find JSON dictionary in response", kind: .invalidJSONResponse)
                }
            }
    }
    
    public func postOAuth2BearerToken() -> RawDataPromise {
        let path = "oauth2/token"
        
        var parameters = Dictionary<String, Any>()
        parameters["grant_type"] = "client_credentials"
        
        return self.jsonRequest(path: path, baseURL: .oauth, method: .POST, parameters: parameters)
    }
    
    public func invalidateOAuth2BearerToken() -> TokenPromise {
        let path = "oauth2/invalidate_token"
        
        return self.jsonRequest(path: path, baseURL: .oauth, method: .POST, parameters: [:])
            .decodeJSON()
            .map { json, response in
                if let accessToken = json["access_token"].string {
                    self.client.credential = nil
                    let credentialToken = Credential.OAuthAccessToken(key: accessToken, secret: "")
                    return (credentialToken, response)
                } else {
                    return (nil, response)
                }
            }
    }
    
    public func postOAuthRequestToken(with callbackURL: URL) -> TokenPromise {
        let path = "oauth/request_token"
        let parameters: [String: Any] =  ["oauth_callback": callbackURL.absoluteString]

        let promise = RawPromise<(Credential.OAuthAccessToken?, URLResponse)>()
        
        promise.request = self.client.post(path, baseURL: .oauth, parameters: parameters, uploadProgress: nil, downloadProgress: nil, success: { data, response in
            let responseString = String(data: data, encoding: .utf8)!
            let accessToken = Credential.OAuthAccessToken(queryString: responseString)
            promise.succeed(with: (accessToken, response))
        }, failure: promise.fail(with:))

        return promise
    }
    
    public func postOAuthAccessToken(with requestToken: Credential.OAuthAccessToken) -> TokenPromise {
        let promise = RawPromise<(Credential.OAuthAccessToken?, URLResponse)>()
        if let verifier = requestToken.verifier {
            let path =  "oauth/access_token"
            let parameters: [String: Any] = ["oauth_token": requestToken.key, "oauth_verifier": verifier]
            
            promise.request = self.client.post(path, baseURL: .oauth, parameters: parameters, uploadProgress: nil, downloadProgress: nil, success: { data, response in
                let responseString = String(data: data, encoding: .utf8)!
                let accessToken = Credential.OAuthAccessToken(queryString: responseString)
                promise.succeed(with: (accessToken, response))
            }, failure: promise.fail(with:))
        } else {
            let error = SwifterError(message: "Bad OAuth response received from server", kind: .badOAuthResponse)
            promise.fail(with: error)
        }
        return promise
    }
    
}
