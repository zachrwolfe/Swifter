//
//  SwifterStreaming.swift
//  Swifter
//
//  Generated on 2018-03-06. DO NOT EDIT.
//  Copyright © 2018 Matt Donnelly. All rights reserved.
//

extension Swifter {

    /**
    POST statuses/filter

    Returns public statuses that match one or more filter predicates. Multiple parameters may be specified which allows most clients to use a single connection to the Streaming API. Both GET and POST requests are supported, but GET requests with too many parameters may cause the request to be rejected for excessive URL length. Use a POST request to avoid long URLs.

    The track, follow, and locations fields should be considered to be combined with an OR operator. track=foo&follow=1234 returns Tweets matching "foo" OR created by user 1234.

    The default access level allows up to 400 track keywords, 5,000 follow userids and 25 0.1-360 degree location boxes. If you need elevated access to the Streaming API, you should explore our partner providers of Twitter data here: https://dev.twitter.com/programs/twitter-certified-products/products#Certified-Data-Products

    At least one predicate parameter (follow, locations, or track) must be specified.
    */
    public func postTweetFilters(follow: [String]? = nil, track: [String]? = nil, locations: [String]? = nil, delimited: Bool? = nil, stallWarnings: Bool? = nil, filterLevel: String? = nil, language: [String]? = nil) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["stall_warnings"] ??= stallWarnings
        parameters["track"] ??= track?.joined(separator: ",")
        parameters["delimited"] ??= delimited
        parameters["follow"] ??= follow?.joined(separator: ",")
        parameters["locations"] ??= locations?.joined(separator: ",")
        parameters["filter_level"] ??= filterLevel
        parameters["language"] ??= language?.joined(separator: ",")

        return self.postJSON(path: "statuses/filter.json", baseURL: .stream, parameters: parameters, requestMode: .streaming)
            .map { data, _ in data }
    }

    /**
    GET statuses/sample

    Returns a small random sample of all public statuses. The Tweets returned by the default access level are the same, so if two different clients connect to this endpoint, they will see the same Tweets.
    */
    public func streamRandomSampleTweets(delimited: Bool? = nil, stallWarnings: Bool? = nil, filterLevel: String? = nil, language: [String]? = nil) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["filter_level"] ??= filterLevel
        parameters["stall_warnings"] ??= stallWarnings
        parameters["delimited"] ??= delimited
        parameters["language"] ??= language?.joined(separator: ",")

        return self.getJSON(path: "statuses/sample.json", baseURL: .stream, parameters: parameters, requestMode: .streaming)
            .map { data, _ in data }
    }

    /**
    GET statuses/firehose

    This endpoint requires special permission to access.

    Returns all public statuses. Few applications require this level of access. Creative use of a combination of other resources and various access levels can satisfy nearly every application use case.
    */
    public func streamFirehoseTweets(count: Int? = nil, delimited: Bool? = nil, stallWarnings: Bool? = nil, filterLevel: String? = nil, language: [String]? = nil) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["filter_level"] ??= filterLevel
        parameters["count"] ??= count
        parameters["stall_warnings"] ??= stallWarnings
        parameters["delimited"] ??= delimited
        parameters["language"] ??= language?.joined(separator: ",")

        return self.getJSON(path: "statuses/firehose.json", baseURL: .stream, parameters: parameters, requestMode: .streaming)
            .map { data, _ in data }
    }

    /**
    GET user

    Streams messages for a single user, as described in User streams https://dev.twitter.com/docs/streaming-apis/streams/user
    */
    public func beginUserStream(delimited: Bool? = nil, stallWarnings: Bool? = nil, includeMessagesFromUserOnly: Bool = false, includeReplies: Bool = false, track: [String]? = nil, locations: [String]? = nil, stringifyFriendIDs: Bool? = nil, filterLevel: String? = nil, language: [String]? = nil) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["with"] ??= includeMessagesFromUserOnly ? "user" : nil
        parameters["stall_warnings"] ??= stallWarnings
        parameters["delimited"] ??= delimited
        parameters["replies"] ??= includeReplies ? "all" : nil
        parameters["track"] ??= track?.joined(separator: ",")
        parameters["locations"] ??= locations?.joined(separator: ",")
        parameters["filter_level"] ??= filterLevel
        parameters["stringify_friend_ids"] ??= stringifyFriendIDs
        parameters["language"] ??= language?.joined(separator: ",")

        return self.getJSON(path: "user.json", baseURL: .userStream, parameters: parameters, requestMode: .streaming)
            .map { data, _ in data }
    }

    /**
    GET site

    Streams messages for a set of users, as described in Site streams https://dev.twitter.com/docs/streaming-apis/streams/site
    */
    public func beginSiteStream(delimited: Bool? = nil, stallWarnings: Bool? = nil, restrictToUserMessages: Bool = false, includeReplies: Bool = false, stringifyFriendIDs: Bool? = nil) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["with"] ??= restrictToUserMessages ? "user" : nil
        parameters["stall_warnings"] ??= stallWarnings
        parameters["delimited"] ??= delimited
        parameters["replies"] ??= includeReplies ? "all" : nil
        parameters["stringify_friend_ids"] ??= stringifyFriendIDs

        return self.getJSON(path: "site.json", baseURL: .stream, parameters: parameters, requestMode: .streaming)
            .map { data, _ in data }
    }

}
