//
//  SwifterTag.swift
//  Swifter
//
//  Created by Andy on 7/30/16.
//  Copyright © 2016 Matt Donnelly. All rights reserved.
//

import Foundation

public enum UserTag {
    case id(UInt64)
    case screenName(String)
    
    var key: String {
        switch self {
        case .id:           return "user_id"
        case .screenName:   return "screen_name"
        }
    }
    
    var value: String {
        switch self {
        case .id(let id):           return String(id)
        case .screenName(let user): return user
        }
    }

    var ownerKey: String {
        switch self {
        case .id:           return "owner_id"
        case .screenName:   return "owner_screen_name"
        }
    }
}

public enum UsersTag {
    case id([UInt64])
    case screenName([String])
    
    var key: String {
        switch self {
        case .id:           return "user_id"
        case .screenName:   return "screen_name"
        }
    }
    
    var value: String {
        switch self {
        case .id(let ids):           return ids.map(String.init).joined(separator: ",")
        case .screenName(let users): return users.joined(separator: ",")
        }
    }
}

public enum ListTag {
    case id(UInt64)
    case slug(String, owner: UserTag)
    
    var key: String {
        switch self {
        case .id:   return "list_id"
        case .slug: return "slug"
        }
    }
    
    var value: String {
        switch self {
        case .id(let id):           return String(id)
        case .slug(let slug, _):    return slug
        }
    }

    var ownerKey: String? {
        switch self {
        case .slug(_, let owner): return owner.ownerKey
        default:                  return nil
        }
    }
    
    var ownerValue: String? {
        switch self {
        case .slug(_, let owner): return owner.ownerKey
        default:                  return nil
        }
    }
}
