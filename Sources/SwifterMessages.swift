//
//  SwifterMessages.swift
//  Swifter
//
//  Generated on 2018-03-06. DO NOT EDIT.
//  Copyright © 2018 Matt Donnelly. All rights reserved.
//

extension Swifter {

    /**
    GET direct_messages

    Returns the 20 most recent direct messages sent to the authenticating user. Includes detailed information about the sender and recipient user. You can request up to 200 direct messages per call, up to a maximum of 800 incoming DMs.
    */
    public func getDirectMessages(since sinceID: UInt64? = nil, maxID: UInt64? = nil, count: Int? = nil, includeEntities: Bool? = nil, skipStatus: Bool? = nil) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["skip_status"] ??= skipStatus
        parameters["since_id"] ??= sinceID
        parameters["count"] ??= count
        parameters["max_id"] ??= maxID
        parameters["include_entities"] ??= includeEntities

        return self.getJSON(path: "direct_messages.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    GET direct_messages/sent

    Returns the 20 most recent direct messages sent by the authenticating user. Includes detailed information about the sender and recipient user. You can request up to 200 direct messages per call, up to a maximum of 800 outgoing DMs.
    */
    public func getSentDirectMessages(since sinceID: UInt64? = nil, maxID: UInt64? = nil, count: Int? = nil, page: Int? = nil, includeEntities: Bool? = nil) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["since_id"] ??= sinceID
        parameters["count"] ??= count
        parameters["max_id"] ??= maxID
        parameters["page"] ??= page
        parameters["include_entities"] ??= includeEntities

        return self.getJSON(path: "direct_messages/sent.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    GET direct_messages/show

    Returns a single direct message, specified by an id parameter. Like the /1.1/direct_messages.format request, this method will include the user objects of the sender and recipient.
    */
    public func showDirectMessage(forID id: UInt64) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["id"] ??= id

        return self.getJSON(path: "direct_messages/show.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    POST direct_messages/destroy

    Destroys the direct message specified in the required ID parameter. The authenticating user must be the recipient of the specified direct message.
    */
    public func destroyDirectMessage(forID id: UInt64, includeEntities: Bool? = nil) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["id"] ??= id
        parameters["include_entities"] ??= includeEntities

        return self.postJSON(path: "direct_messages/destroy.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    POST direct_messages/new

    Sends a new direct message to the specified user from the authenticating user. Requires both the user and text parameters and must be a POST. Returns the sent message in the requested format if successful.
    */
    public func sendDirectMessage(to userTag: UserTag, text: String) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters[userTag.key] ??= userTag.value
        parameters["text"] ??= text

        return self.postJSON(path: "direct_messages/new.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

}
