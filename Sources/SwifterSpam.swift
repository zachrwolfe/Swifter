//
//  SwifterSpam.swift
//  Swifter
//
//  Generated on 2018-03-06. DO NOT EDIT.
//  Copyright © 2018 Matt Donnelly. All rights reserved.
//

extension Swifter {

    /**
    POST users/report_spam

    Report the specified user as a spam account to Twitter. Additionally performs the equivalent of POST blocks/create on behalf of the authenticated user.
    */
    public func reportSpam(for userTag: UserTag) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters[userTag.key] ??= userTag.value

        return self.postJSON(path: "users/report_spam.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

}
