//
//  SwifterGIF.swift
//  Swifter
//
//  Created by Justin Greenfield on 6/30/17.
//  Copyright © 2017 Matt Donnelly. All rights reserved.
//

import Foundation


public enum GIFterError: Error {
	case invalidData, invalidResponse
}

public extension Swifter {
	
	func tweetGIF(_ text: String, attachment: URL) -> DataPromise {
		guard let data = try? Data(contentsOf: attachment) else {
            return Promise<Data>.failingPromise(with: GIFterError.invalidData)
        }
		return prepareUpload(data: data).decodeJSON().map { json in json["media_id_string"].string! }
            .chainDiscardingData { mediaID in
                self.uploadGIF(mediaID, data: data, name: attachment.lastPathComponent)
            }
            .chainDiscardingData { mediaID in
                self.finalizeUpload(mediaID: mediaID)
            }
            .chain { mediaID in
                self.postTweet(status: text, mediaIDs: [mediaID])
            }
	}
	
	private func prepareUpload(data: Data) -> DataPromise {
		let path = "media/upload.json"
		let parameters : [String:Any] = ["command": "INIT", "total_bytes": data.count,
		                  "media_type": "image/gif", "media_category": "tweet_gif"]
        return self.postJSON(path: path, baseURL: .upload, parameters: parameters)
            .map { json, _ in json }
	}
	
	private func finalizeUpload(mediaID: String) -> DataPromise {
		let path = "media/upload.json"
		let parameters = ["command": "FINALIZE", "media_id" : mediaID]
		return self.postJSON(path: path, baseURL: .upload, parameters: parameters)
            .map { json, _ in json }
	}
	
	private func uploadGIF(_ mediaID: String, data: Data, name: String? = nil) -> DataPromise {
		let path = "media/upload.json"
		let parameters : [String:Any] = ["command": "APPEND", "media_id": mediaID, "segment_index": "0",
		                  Swifter.DataParameters.dataKey : "media",
		                  Swifter.DataParameters.fileNameKey: name ?? "GIFter.gif",
		                  "media": data]
		return self.postJSON(path: path, baseURL: .upload, parameters: parameters)
            .map { json, _ in json }
	}
	
}
