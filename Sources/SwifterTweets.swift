//
//  SwifterTweets.swift
//  Swifter
//
//  Generated on 2018-03-06. DO NOT EDIT.
//  Copyright © 2018 Matt Donnelly. All rights reserved.
//

import Foundation

extension Swifter {

    /**
    GET statuses/retweets/:id

    Returns a single Tweet, specified by the id parameter. The Tweet's author will also be embedded within the tweet.

    See Embeddable Timelines, Embeddable Tweets, and GET statuses/oembed for tools to render Tweets according to Display Requirements.

    # About Geo

    If there is no geotag for a status, then there will be an empty <geo/> or "geo" : {}. This can only be populated if the user has used the Geotagging API to send a statuses/update.

    The JSON response mostly uses conventions laid out in GeoJSON. Unfortunately, the coordinates that Twitter renders are reversed from the GeoJSON specification (GeoJSON specifies a longitude then a latitude, whereas we are currently representing it as a latitude then a longitude). Our JSON renders as: "geo": { "type":"Point", "coordinates":[37.78029, -122.39697] }

    # Contributors

    If there are no contributors for a Tweet, then there will be an empty or "contributors" : {}. This field will only be populated if the user has contributors enabled on his or her account -- this is a beta feature that is not yet generally available to all.

    This object contains an array of user IDs for users who have contributed to this status (an example of a status that has been contributed to is this one). In practice, there is usually only one ID in this array. The JSON renders as such "contributors":[8285392].
    */
    public func getRetweets(forTweetID id: UInt64, count: Int? = nil, trimUser: Bool? = nil, tweetMode: TweetMode = TweetMode.default) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["id"] ??= id
        parameters["tweet_mode"] ??= tweetMode.stringValue
        parameters["count"] ??= count
        parameters["trim_user"] ??= trimUser

        return self.getJSON(path: "statuses/retweets/\(id).json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    GET statuses/show

    Destroys the status specified by the required ID parameter. The authenticating user must be the author of the specified status. Returns the destroyed status if successful.
    */
    public func getTweet(forID id: UInt64, count: Int? = nil, trimUser: Bool? = nil, includeMyRetweet: Bool? = nil, includeEntities: Bool? = nil, tweetMode: TweetMode = TweetMode.default) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["id"] ??= id
        parameters["include_entities"] ??= includeEntities
        parameters["count"] ??= count
        parameters["tweet_mode"] ??= tweetMode.stringValue
        parameters["include_my_retweet"] ??= includeMyRetweet
        parameters["trim_user"] ??= trimUser

        return self.getJSON(path: "statuses/show.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    POST statuses/destroy/:id

    Updates the authenticating user's current status, also known as tweeting. To upload an image to accompany the tweet, use POST media/upload.

    For each update attempt, the update text is compared with the authenticating user's recent tweets. Any attempt that would result in duplication will be blocked, resulting in a 403 error. Therefore, a user cannot submit the same status twice in a row.

    While not rate limited by the API a user is limited in the number of tweets they can create at a time. If the number of updates posted by the user reaches the current allowed limit this method will return an HTTP 403 error.

    - Any geo-tagging parameters in the update will be ignored if geo_enabled for the user is false (this is the default setting for all users unless the user has enabled geolocation in their settings)
    - The number of digits passed the decimal separator passed to lat, up to 8, will be tracked so that the lat is returned in a status object it will have the same number of digits after the decimal separator.
    - Please make sure to use to use a decimal point as the separator (and not the decimal comma) for the latitude and the longitude - usage of the decimal comma will cause the geo-tagged portion of the status update to be dropped.
    - For JSON, the response mostly uses conventions described in GeoJSON. Unfortunately, the geo object has coordinates that Twitter renderers are reversed from the GeoJSON specification (GeoJSON specifies a longitude then a latitude, whereas we are currently representing it as a latitude then a longitude. Our JSON renders as: "geo": { "type":"Point", "coordinates":[37.78217, -122.40062] }
    - The coordinates object is replacing the geo object (no deprecation date has been set for the geo object yet) -- the difference is that the coordinates object, in JSON, is now rendered correctly in GeoJSON.
    - If a place_id is passed into the status update, then that place will be attached to the status. If no place_id was explicitly provided, but latitude and longitude are, we attempt to implicitly provide a place by calling geo/reverse_geocode.
    - Users will have the ability, from their settings page, to remove all the geotags from all their tweets en masse. Currently we are not doing any automatic scrubbing nor providing a method to remove geotags from individual tweets.

    See:

    - https://dev.twitter.com/notifications/multiple-media-entities-in-tweets
    - https://dev.twitter.com/docs/api/multiple-media-extended-entities
    */
    public func destroyTweet(forID id: UInt64, trimUser: Bool? = nil, tweetMode: TweetMode = TweetMode.default) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["id"] ??= id
        parameters["tweet_mode"] ??= tweetMode.stringValue
        parameters["trim_user"] ??= trimUser

        return self.postJSON(path: "statuses/destroy/\(id).json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    POST statuses/update

    Upload media (images) to Twitter for use in a Tweet or Twitter-hosted Card. For uploading videos or for chunked image uploads (useful for lower bandwidth connections), see our chunked POST media/upload endpoint.
    */
    public func postTweet(status: String, inReplyToStatusID: UInt64? = nil, coordinate: (lat: Double, long: Double)? = nil, placeID: Double? = nil, displayCoordinates: Bool? = nil, trimUser: Bool? = nil, mediaIDs: [String] = [], attachmentURL: URL? = nil, tweetMode: TweetMode = TweetMode.default) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["media_ids"] ??= !mediaIDs.isEmpty ? mediaIDs.joined(separator: ",") : nil
        parameters["long"] ??= coordinate?.long
        parameters["in_reply_to_status_id"] ??= inReplyToStatusID
        parameters["place_id"] ??= placeID
        parameters["display_coordinates"] ??= displayCoordinates
        parameters["status"] ??= status
        parameters["tweet_mode"] ??= tweetMode.stringValue
        parameters["lat"] ??= coordinate?.lat
        parameters["attachment_url"] ??= attachmentURL
        parameters["trim_user"] ??= trimUser

        return self.postJSON(path: "statuses/update.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    public func postMedia(_ media: Data, additionalOwners: UsersTag? = nil) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["media"] ??= media
        parameters["additional_owners"] ??= additionalOwners?.value

        return self.postJSON(path: "media/upload.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    POST statuses/retweet/:id

    Retweets a tweet. Returns the original tweet with retweet details embedded.

    - This method is subject to update limits. A HTTP 403 will be returned if this limit as been hit.
    - Twitter will ignore attempts to perform duplicate retweets.
    - The retweet_count will be current as of when the payload is generated and may not reflect the exact count. It is intended as an approximation.

    Returns Tweets (1: the new tweet)
    */
    public func retweetTweet(forID id: UInt64, trimUser: Bool? = nil, tweetMode: TweetMode = TweetMode.default) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["id"] ??= id
        parameters["tweet_mode"] ??= tweetMode.stringValue
        parameters["trim_user"] ??= trimUser

        return self.postJSON(path: "statuses/retweet/\(id).json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    POST statuses/unretweet/:id

    Untweets a retweeted status. Returns the original Tweet with retweet details embedded.

    - This method is subject to update limits. A HTTP 429 will be returned if this limit has been hit.
    - The untweeted retweet status ID must be authored by the user backing the authentication token.
    - An application must have write privileges to POST. A HTTP 401 will be returned for read-only applications.
    - When passing a source status ID instead of the retweet status ID a HTTP 200 response will be returned with the same Tweet object but no action.

    Returns Tweets (1: the original tweet)
    */
    public func unretweetTweet(forID id: UInt64, trimUser: Bool? = nil, tweetMode: TweetMode = TweetMode.default) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["id"] ??= id
        parameters["tweet_mode"] ??= tweetMode.stringValue
        parameters["trim_user"] ??= trimUser

        return self.postJSON(path: "statuses/unretweet/\(id).json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    GET statuses/oembed

    Returns information allowing the creation of an embedded representation of a Tweet on third party sites. See the oEmbed specification for information about the response format.

    While this endpoint allows a bit of customization for the final appearance of the embedded Tweet, be aware that the appearance of the rendered Tweet may change over time to be consistent with Twitter's Display Requirements. Do not rely on any class or id parameters to stay constant in the returned markup.
    */
    public func oembedInfo(forID id: UInt64, maxWidth: Int? = nil, hideMedia: Bool? = nil, hideThread: Bool? = nil, omitScript: Bool? = nil, align: String? = nil, related: String? = nil, lang: String? = nil) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["id"] ??= id
        parameters["lang"] ??= lang
        parameters["max_width"] ??= maxWidth
        parameters["hide_media"] ??= hideMedia
        parameters["align"] ??= align
        parameters["hide_thread"] ??= hideThread
        parameters["related"] ??= related
        parameters["omit_script"] ??= omitScript

        return self.getJSON(path: "statuses/oembed.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    public func oembedInfo(forURL url: URL, maxWidth: Int? = nil, hideMedia: Bool? = nil, hideThread: Bool? = nil, omitScript: Bool? = nil, align: String? = nil, related: String? = nil, lang: String? = nil) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["lang"] ??= lang
        parameters["max_width"] ??= maxWidth
        parameters["hide_media"] ??= hideMedia
        parameters["align"] ??= align
        parameters["url"] ??= url.absoluteString
        parameters["hide_thread"] ??= hideThread
        parameters["related"] ??= related
        parameters["omit_script"] ??= omitScript

        return self.getJSON(path: "statuses/oembed.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    GET statuses/retweeters/ids

    Returns a collection of up to 100 user IDs belonging to users who have retweeted the tweet specified by the id parameter.

    This method offers similar data to GET statuses/retweets/:id and replaces API v1's GET statuses/:id/retweeted_by/ids method.
    */
    public func tweetRetweeters(forID id: UInt64, count: Int? = nil, cursor: String? = nil, stringifyIDs: Bool? = nil) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["id"] ??= id
        parameters["count"] ??= count
        parameters["cursor"] ??= cursor
        parameters["stringify_ids"] ??= stringifyIDs

        return self.getJSON(path: "statuses/retweeters/ids.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    POST statuses/lookup

    Returns fully-hydrated tweet objects for up to 100 tweets per request, as specified by comma-separated values passed to the id parameter. This method is especially useful to get the details (hydrate) a collection of Tweet IDs. GET statuses/show/:id is used to retrieve a single tweet object.
    */
    public func lookupTweets(for tweetIDs: [String], includeEntities: Bool? = nil, map: Bool? = nil, tweetMode: TweetMode = TweetMode.default) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["id"] ??= tweetIDs.joined(separator: ",")
        parameters["tweet_mode"] ??= tweetMode.stringValue
        parameters["map"] ??= map
        parameters["include_entities"] ??= includeEntities

        return self.postJSON(path: "statuses/lookup.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

}
