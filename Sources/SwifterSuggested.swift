//
//  SwifterSuggested.swift
//  Swifter
//
//  Generated on 2018-03-06. DO NOT EDIT.
//  Copyright © 2018 Matt Donnelly. All rights reserved.
//

extension Swifter {

    /**
    GET users/suggestions/:slug

    Access the users in a given category of the Twitter suggested user list.

    It is recommended that applications cache this data for no more than one hour.
    */
    public func getUserSuggestions(slug: String, lang: String? = nil) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["lang"] ??= lang

        return self.getJSON(path: "users/suggestions/\(slug).json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    GET users/suggestions

    Access to Twitter's suggested user list. This returns the list of suggested user categories. The category can be used in GET users/suggestions/:slug to get the users in that category.
    */
    public func getUsersSuggestions(lang: String? = nil) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["lang"] ??= lang

        return self.getJSON(path: "users/suggestions.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    GET users/suggestions/:slug/members

    Access the users in a given category of the Twitter suggested user list and return their most recent status if they are not a protected user.
    */
    public func getUsersSuggestions(for slug: String) -> DataPromise {
        return self.getJSON(path: "users/suggestions/\(slug)/members.json", baseURL: .api, parameters: [:])
            .map { data, _ in data }
    }

}
