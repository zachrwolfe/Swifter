//
//  SwifterTimelines.swift
//  Swifter
//
//  Generated on 2018-03-06. DO NOT EDIT.
//  Copyright © 2018 Matt Donnelly. All rights reserved.
//

extension Swifter {

    /**
    GET statuses/mentions_timeline

    Returns Tweets (*: mentions for the user)

    Returns the 20 most recent mentions (tweets containing a users's @screen_name) for the authenticating user.

    The timeline returned is the equivalent of the one seen when you view your mentions on twitter.com.

    This method can only return up to 800 tweets.
    */
    public func getMentionsTimelineTweets(count: Int? = nil, sinceID: UInt64? = nil, maxID: UInt64? = nil, trimUser: Bool? = nil, contributorDetails: Bool? = nil, includeEntities: Bool? = nil, tweetMode: TweetMode = TweetMode.default) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["trim_user"] ??= trimUser
        parameters["since_id"] ??= sinceID
        parameters["contributor_details"] ??= contributorDetails
        parameters["tweet_mode"] ??= tweetMode.stringValue
        parameters["count"] ??= count
        parameters["max_id"] ??= maxID
        parameters["include_entities"] ??= includeEntities

        return self.getJSON(path: "statuses/mentions_timeline.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    GET statuses/user_timeline

    Returns Tweets (*: tweets for the user)

    Returns a collection of the most recent Tweets posted by the user indicated by the screen_name or user_id parameters.

    User timelines belonging to protected users may only be requested when the authenticated user either "owns" the timeline or is an approved follower of the owner.

    The timeline returned is the equivalent of the one seen when you view a user's profile on twitter.com.

    This method can only return up to 3,200 of a user's most recent Tweets. Native retweets of other statuses by the user is included in this total, regardless of whether include_rts is set to false when requesting this resource.
    */
    public func getTimeline(for userID: UInt64, count: Int? = nil, sinceID: UInt64? = nil, maxID: UInt64? = nil, trimUser: Bool? = nil, contributorDetails: Bool? = nil, includeEntities: Bool? = nil, tweetMode: TweetMode = TweetMode.default) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["include_entities"] ??= includeEntities
        parameters["since_id"] ??= sinceID
        parameters["contributor_details"] ??= contributorDetails
        parameters["user_id"] ??= userID
        parameters["tweet_mode"] ??= tweetMode.stringValue
        parameters["count"] ??= count
        parameters["max_id"] ??= maxID
        parameters["trim_user"] ??= trimUser

        return self.getJSON(path: "statuses/user_timeline.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    GET statuses/home_timeline

    Returns Tweets (*: tweets from people the user follows)

    Returns a collection of the most recent Tweets and retweets posted by the authenticating user and the users they follow. The home timeline is central to how most users interact with the Twitter service.

    Up to 800 Tweets are obtainable on the home timeline. It is more volatile for users that follow many users or follow users who tweet frequently.
    */
    public func getHomeTimeline(count: Int? = nil, sinceID: UInt64? = nil, maxID: UInt64? = nil, trimUser: Bool? = nil, contributorDetails: Bool? = nil, includeEntities: Bool? = nil, tweetMode: TweetMode = TweetMode.default) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["trim_user"] ??= trimUser
        parameters["since_id"] ??= sinceID
        parameters["contributor_details"] ??= contributorDetails
        parameters["tweet_mode"] ??= tweetMode.stringValue
        parameters["count"] ??= count
        parameters["max_id"] ??= maxID
        parameters["include_entities"] ??= includeEntities

        return self.getJSON(path: "statuses/home_timeline.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

    /**
    GET statuses/retweets_of_me

    Returns the most recent tweets authored by the authenticating user that have been retweeted by others. This timeline is a subset of the user's GET statuses/user_timeline. See Working with Timelines for instructions on traversing timelines.
    */
    public func getRetweetsOfMe(count: Int? = nil, sinceID: UInt64? = nil, maxID: UInt64? = nil, trimUser: Bool? = nil, contributorDetails: Bool? = nil, includeEntities: Bool? = nil, tweetMode: TweetMode = TweetMode.default) -> DataPromise {
        var parameters: [String: Any] = [:]
        parameters["trim_user"] ??= trimUser
        parameters["since_id"] ??= sinceID
        parameters["contributor_details"] ??= contributorDetails
        parameters["tweet_mode"] ??= tweetMode.stringValue
        parameters["count"] ??= count
        parameters["max_id"] ??= maxID
        parameters["include_entities"] ??= includeEntities

        return self.getJSON(path: "statuses/retweets_of_me.json", baseURL: .api, parameters: parameters)
            .map { data, _ in data }
    }

}
