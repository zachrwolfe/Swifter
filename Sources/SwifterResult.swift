//
//  SwifterResult.swift
//  Swifter
//
//  Copyright (c) 2014 Matt Donnelly.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

import Foundation

public enum Result<Wrapped> {
    case success(Wrapped)
    case failure(Error)

    public func unwrapped() throws -> Wrapped {
        switch self {
        case let .success(wrapped): return wrapped
        case let .failure(error):   throw error
        }
    }

    public func map<T>(_ transform: (Wrapped) throws -> T) -> Result<T> {
        switch self {
        case let .success(wrapped):
            do {
                return .success(try transform(wrapped))
            } catch {
                return .failure(error)
            }
        case let .failure(error): return .failure(error)
        }
    }

    public func flatMap<T>(_ transform: (Wrapped) -> T) -> Result<T> {
        switch self {
        case let .success(wrapped): return .success(transform(wrapped))
        case let .failure(error):   return .failure(error)
        }
    }
}

extension Result where Wrapped == Data {
    public func decoding<T: Decodable>(_ type: T.Type) -> Result<T> {
        return map { data in try Swifter.decoder.decode(type, from: data) }
    }

    public func decodingJSON() -> Result<JSON> {
        return map { data in try JSON.parse(jsonData: data) }
    }
}

extension Result where Wrapped == (Data, HTTPURLResponse) {
    public func decoding<T: Decodable>(_ type: T.Type) -> Result<(T, HTTPURLResponse)> {
        return map { arg in
            let (data, response) = arg
            return (try Swifter.decoder.decode(type, from: data), response)
        }
    }

    public func decodingJSON() -> Result<(JSON, HTTPURLResponse)> {
        return map { arg in
            let (data, response) = arg
            return (try JSON.parse(jsonData: data), response)
        }
    }
}
