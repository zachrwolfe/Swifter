//
//  Tokenizer.swift
//  Preprocessor
//
//  Created by Zach Wolfe on 2018-01-14.
//

import Foundation

class Preprocessor {
    private let codeGenerator: CodeGenerator

    init(fileAt url: URL) throws {
        guard let metadata = try? String(contentsOf: url) else {
            fatalError("Failed to read metadata file at \(url)")
        }

        codeGenerator = CodeGenerator(tokenizer: Tokenizer(metadata: metadata))
    }

    func outputFiles(at url: URL) throws {
        if codeGenerator.files.isEmpty {
            try codeGenerator.generateCode()
        }
        for (fileName, file) in codeGenerator.files {
            let outputURL = url.appendingPathComponent(fileName)
            try file.write(to: outputURL, atomically: true, encoding: .utf8)
        }
    }

    func cleanFiles(at url: URL) throws {
        if codeGenerator.files.isEmpty {
            try codeGenerator.generateCode()
        }
        for (fileName, _) in codeGenerator.files {
            let deleteURL = url.appendingPathComponent(fileName)
            try FileManager.default.removeItem(at: deleteURL)
        }
    }
}
