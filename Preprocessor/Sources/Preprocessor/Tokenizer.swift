//
//  Tokenizer.swift
//  Preprocessor
//
//  Created by Zach Wolfe on 2017-12-17.
//

import Foundation

enum Token: Equatable {
    case string(String)
    case keyword(Keyword)
    case expressionLiteral(String)
    case semicolon
    case openParentheses
    case closeParentheses
    case colon
    case comma
    case equal
    case slash

    static var separators: [Character: Token] {
        return [
            ";": .semicolon,
            "(": .openParentheses,
            ")": .closeParentheses,
            ":": .colon,
            ",": .comma,
            "=": .equal,
            "/": .slash
        ]
    }

    static func isSeparator(_ char: Character) -> Bool {
        return separators.keys.contains(char)
    }

    static func isIdentifierTerminator(_ char: Character) -> Bool {
        return isSeparator(char) || char == "{" || char == "}" || char == "\""
    }

    static var keywords: [Keyword] {
        return [.file, .get, .cursorids, .cursorusers, .cursorlists, .post, .getstream, .poststream, .userstream, .searchmethod, .importing]
    }

    enum Keyword: String {
        case file
        case get
        case post
        case getstream
        case poststream
        case userstream
        case cursorids
        case cursorusers
        case cursorlists
        case searchmethod

        case importing
    }
}

final class Tokenizer {
    let metadata: String
    init(metadata: String) { self.metadata = metadata }

    private var index: String.Index = String.Index(encodedOffset: 0)
    var token: Token? {
        didSet {
            previousToken = oldValue
        }
    }

    private func skipWhitespace() {
        while index < metadata.endIndex, isWhitespace(metadata[index]) {
            index = metadata.index(after: index)
        }
    }

    private func isWhitespace(_ char: Character) -> Bool{
        return char.unicodeScalars.reduce(true) {
            $0 && CharacterSet.whitespacesAndNewlines.contains($1)
        }
    }

    var hasMoreTokens: Bool {
        skipWhitespace()
        return index < metadata.endIndex
    }

    var previousToken: Token?

    @discardableResult
    func advance() throws -> Bool {
        guard hasMoreTokens else {
            token = nil
            return false
        }

        // maybe it's a separator character
        for (char, token) in Token.separators {
            if metadata[index] == char {
                index = metadata.index(after: index)
                self.token = token
                return true
            }
        }

        // maybe it's a string literal
        if metadata[index] == "\"" {
            var literal = ""
            var prevIndex = index
            index = metadata.index(after: index)
            while index < metadata.endIndex {
                if metadata[index] == "\"" {
                    if metadata[prevIndex] == "\\" {
                        // remove the backslash used to escape the quotation
                        literal.removeLast()
                    } else {
                        index = metadata.index(after: index)
                        token = .string(literal)
                        return true
                    }
                }

                literal.append(metadata[index])
                prevIndex = index
                index = metadata.index(after: index)
            }
            // we reached the end of file
            throw TokenizerError.invalidStringLiteral
        }

        // maybe it's an expression literal
        if metadata[index] == "{" {
            var literal = ""
            var nestLevels = 0
            index = metadata.index(after: index)
            while index < metadata.endIndex {
                if metadata[index] == "{" {
                    nestLevels += 1
                } else if metadata[index] == "}" {
                    nestLevels -= 1
                    if nestLevels < 0 {
                        index = metadata.index(after: index)
                        token = .expressionLiteral(literal)
                        return true
                    }
                }

                literal.append(metadata[index])
                index = metadata.index(after: index)
            }
        }

        // maybe it's a keyword
        for keyword in Token.keywords {
            if metadata[index...].hasPrefix(keyword.rawValue) {
                let afterKeywordIndex = metadata.index(index, offsetBy: keyword.rawValue.count)
                if
                    isWhitespace(metadata[afterKeywordIndex]) ||
                    Token.isIdentifierTerminator(metadata[afterKeywordIndex])
                {
                    index = afterKeywordIndex
                    token = .keyword(keyword)
                    return true
                }
            }
        }

        // must be an identifier
        var identifier = String(metadata[index])
        index = metadata.index(after: index)
        while index < metadata.endIndex {
            if
                isWhitespace(metadata[index]) ||
                Token.isIdentifierTerminator(metadata[index])
            {
                token = .string(identifier)
                return true
            }

            identifier.append(metadata[index])
            index = metadata.index(after: index)
        }

        throw TokenizerError.unrecognizedToken
    }

    enum TokenizerError: Error {
        case unrecognizedToken
        case invalidStringLiteral
    }
}
