//
//  CodeGenerator.swift
//  Preprocessor
//
//  Created by Zach Wolfe on 2017-12-17.
//

import Foundation

final class CodeGenerator {
    let tokenizer: Tokenizer
    var files: [String: String] = [:]
    private var indentationLevel = 0
    public var currentFile = ""
    init(tokenizer: Tokenizer) {
        self.tokenizer = tokenizer
    }

    // MARK: Code generation
    func generateCode() throws {
        try tokenizer.advance()

        while tokenizer.hasMoreTokens {
            guard tokenizer.token! == .keyword(.file) else {
                throw CodeGeneratorError.unexpectedToken(tokenizer.token!, previousToken: tokenizer.previousToken)
            }
            try genFile()
        }
    }
    private func genFile() throws {
        func fileHeader(forFileNamed fileName: String) -> String {
            let date = Date()
            let dateString = DateFormatter.localizedString(from: date,
                                                           dateStyle: .short,
                                                           timeStyle: .none)
            let year = Calendar.current.component(.year, from: date)
            return """
            //
            //  \(fileName)
            //  Swifter
            //
            //  Generated on \(dateString). DO NOT EDIT.
            //  Copyright © \(year) Matt Donnelly. All rights reserved.
            //
            """
        }

        try advance()
        let fileName = try expectString()
        let fullFileName = "Swifter\(fileName).swift"
        currentFile += fileHeader(forFileNamed: fullFileName)
        writeLine()

        try advance()
        if tokenizer.token! == .keyword(.importing) {
            repeat {
                try advance()
                writeLine("import " + (try expectString()))
                try advance()
            } while tokenizer.token! == .comma
            writeLine()
        }

        writeLine("extension Swifter {")
        indentationLevel += 1
        writeLine()

        while tokenizer.hasMoreTokens, case let .keyword(keyword) = tokenizer.token!, keyword != .file {
            switch keyword {
            case .get: try genRESTMethod(httpMethod: "get")
            case .post: try genRESTMethod(httpMethod: "post")
            case .cursorids: try genCursorRESTMethod(cursorKey: "ids")
            case .cursorusers: try genCursorRESTMethod(cursorKey: "users")
            case .cursorlists: try genCursorRESTMethod(cursorKey: "lists")
            case .getstream: try genStreamingMethod(httpMethod: "get")
            case .poststream: try genStreamingMethod(httpMethod: "post")
            case .userstream: try genStreamingMethod(httpMethod: "get", userStream: true)
            case .searchmethod: try genSearchMethod()
            default: fatalError()
            }
        }

        indentationLevel -= 1
        writeLine("}")
        writeLine()

        files[fullFileName] = currentFile
        currentFile = ""
        indentationLevel = 0
        return
    }
    // this method is called by each of the specific gen_Method methods, and it does the lion's share of
    // parsing the metadata and generating code.
    private func genMethod(extraParameters: String = "", httpMethod: String) throws -> (apiPath: String, parameterExpression: String) {
        func parseParameterList() throws -> (swiftList: String, expressions: [String: String]) {
            var list = ""
            var expressions: [String: String] = [:]
            while tokenizer.token != .closeParentheses {
                let parameterLabel = try expectString()
                let parameterName: String
                try advance()
                if let _parameterName = try? expectString() {
                    parameterName = _parameterName
                    list += universalToCamelCase(parameterLabel) + " " + universalToCamelCase(parameterName)
                    try advance()
                } else {
                    parameterName = parameterLabel
                    list += universalToCamelCase(parameterLabel)
                }

                try expect(.colon)

                try advance()
                let type = try expectString()
                list += ": \(type)"

                try advance()
                if tokenizer.token == .equal {
                    try advance()
                    let defaultArgument = try expectString()
                    list += " = \(defaultArgument)"
                    try advance()
                }

                if let customExpression = try? expectExpressionLiteral() {
                    expressions["\"" + universalToSnakeCase(parameterName) + "\""] = customExpression
                    try advance()
                } else if tokenizer.token == .openParentheses {
                    try advance()
                    while tokenizer.token != .closeParentheses {
                        let subparameterIndex: String
                        if let subparameterName = try? expectString() {
                            subparameterIndex = "\"\(subparameterName)\""
                        } else {
                            subparameterIndex = try expectExpressionLiteral()
                        }

                        try advance()
                        let customExpression = try expectExpressionLiteral()
                        expressions[subparameterIndex] = customExpression

                        try advance()

                        if tokenizer.token == .comma {
                            try advance()
                        }
                    }
                    try expect(.closeParentheses)
                    try advance()
                } else {
                    expressions["\"" + universalToSnakeCase(parameterName) + "\""] = universalToCamelCase(parameterName)
                }

                if tokenizer.token == .comma {
                    list += ", "
                    try advance()
                }
            }

            return (list, expressions)
        }
        try advance()
        var apiPath = universalToSnakeCase(try expectString())
        var docPath = apiPath
        try advance()
        while tokenizer.hasMoreTokens {
            guard tokenizer.token == .slash else { break }
            apiPath += "/"
            docPath += "/"
            try advance()
            if tokenizer.token == .colon {
                try advance()
                let parameter = universalToSnakeCase(try expectString())
                apiPath += "\\(" + parameter + ")"
                docPath += ":" + parameter
                try advance()
            } else {
                let parameter = universalToSnakeCase(try expectString())
                apiPath += parameter
                docPath += parameter
                try advance()
            }
        }
        apiPath += ".json"

        if let curTok = tokenizer.token, case let .expressionLiteral(comments) = curTok {
            let comments = removeLeadingSpacesPerLine(from: comments)
            writeLine("/**")
            writeLine(httpMethod.uppercased() + " " + docPath)
            writeLine()
            for line in comments {
                writeLine(line)
            }
            writeLine("*/")
            try advance()
        }
        let methodName = try expectString()

        writeLine("public func \(methodName)(")

        try advance()
        try expect(.openParentheses)

        try advance()
        let (parameterList, expressions) = try parseParameterList()
        currentFile += parameterList

        if !parameterList.isEmpty && !extraParameters.isEmpty {
            currentFile += ", "
        }
        currentFile += extraParameters

        try expect(.closeParentheses)
        try tokenizer.advance()

        currentFile += ") -> DataPromise {"
        indentationLevel += 1

        let parametersExpression: String
        if !expressions.isEmpty {
            writeLine("var parameters: [String: Any] = [:]")
            for (parameter, expression) in expressions {
                writeLine("parameters[\(parameter)] ??= \(expression)")
            }
            writeLine()
            parametersExpression = "parameters"
        } else {
            parametersExpression = "[:]"
        }
        return (apiPath, parametersExpression)
    }

    // MARK: - Specialized method generators
    private func genStreamingMethod(httpMethod: String, userStream: Bool = false) throws {
        let (apiPath, parametersString) = try genMethod(httpMethod: httpMethod)

        let baseURL: String
        if userStream {
            baseURL = ".userStream"
        } else {
            baseURL = ".stream"
        }
        writeLine("return self.\(httpMethod)JSON(path: \"\(apiPath)\", baseURL: \(baseURL), parameters: \(parametersString), requestMode: .streaming)")
        indentAndWriteLine(".map { data, _ in data }")
        indentationLevel -= 1

        unindentAndWriteLine("}")
        writeLine()
    }
    private func genRESTMethod(httpMethod: String) throws {
        let (apiPath, parametersString) = try genMethod(httpMethod: httpMethod)

        writeLine("return self.\(httpMethod)JSON(path: \"\(apiPath)\", baseURL: .api, parameters: \(parametersString))")
        indentAndWriteLine(".map { data, _ in data }")
        indentationLevel -= 1

        unindentAndWriteLine("}")
        writeLine()
    }
    private func genCursorRESTMethod(cursorKey: String) throws {
        let (apiPath, parametersString) = try genMethod(httpMethod: "get")

        writeLine("return self.getJSON(path: \"\(apiPath)\", baseURL: .api, parameters: \(parametersString))")
        indentAndWriteLine(".map { data, _ in data }")
        indentationLevel -= 1

        unindentAndWriteLine("}")
        writeLine()
    }
    private func genSearchMethod() throws {
        let (apiPath, parametersString) = try genMethod(httpMethod: "get")

        writeLine("return self.getJSON(path: \"\(apiPath)\", baseURL: .api, parameters: \(parametersString))")
        indentAndWriteLine(".map { data, _ in data }")
        indentationLevel -= 1

        unindentAndWriteLine("}")
        writeLine()
    }

    // MARK: - String processing
    private func universalToSnakeCase(_ universalString: String) -> String {
        return universalString.lowercased()
    }
    private func universalToCamelCase(_ universalString: String) -> String {
        if universalString == "_" { return "_" }
        var camelCase = ""
        for (i, segment) in universalString.split(separator: "_").enumerated() {
            if i == 0 {
                camelCase += segment
            } else {
                camelCase += segment.prefix(1).capitalized + segment.dropFirst()
            }
        }
        return camelCase
    }
    private func removeLeadingSpacesPerLine(from string: String) -> [String] {
        var lines = string.split(separator: "\n", omittingEmptySubsequences: false)[0...]
        if lines.first == "" { lines = lines.suffix(lines.count - 1) }
        if lines.last == "" { lines = lines.prefix(lines.count - 1) }

        let minimumNumberOfSpaces = lines.reduce(Int.max) { previousMinimum, line in
            if line.isEmpty { return previousMinimum }
            for (i, character) in line.enumerated() {
                if character != " " { return min(previousMinimum, i) }
            }
            return min(previousMinimum, line.count)
        }

        return lines.map { line in
            if line.isEmpty { return String(line) }
            else {
                return String(line.suffix(from: line.index(line.startIndex, offsetBy: minimumNumberOfSpaces)))
            }
        }
    }

    // MARK: - Line writing
    private func writeLine(_ line: String = "") {
        currentFile += "\n"
        guard !line.isEmpty else { return }
        for _ in 0..<indentationLevel {
            currentFile += "    "
        }
        currentFile += line
    }
    private func indentAndWriteLine(_ line: String) {
        indentationLevel += 1
        writeLine(line)
    }
    private func unindentAndWriteLine(_ line: String) {
        indentationLevel -= 1
        writeLine(line)
    }

    // MARK: - Error-handling abstractions
    private func expectString() throws -> String {
        guard case let .string(string) = tokenizer.token! else {
            throw CodeGeneratorError.unexpectedToken(tokenizer.token!, previousToken: tokenizer.previousToken)
        }
        return string
    }
    private func expectExpressionLiteral() throws -> String {
        guard case let .expressionLiteral(expression) = tokenizer.token! else {
            throw CodeGeneratorError.unexpectedToken(tokenizer.token!, previousToken: tokenizer.previousToken)
        }
        return expression
    }
    private func expect(_ token: Token) throws {
        guard tokenizer.token == token else {
            throw CodeGeneratorError.unexpectedToken(tokenizer.token!, previousToken: tokenizer.previousToken)
        }
    }
    private func advance() throws {
        guard try tokenizer.advance() else { throw CodeGeneratorError.eof }
    }

    enum CodeGeneratorError: Error {
        case unexpectedToken(Token, previousToken: Token?)
        case eof
    }
}
