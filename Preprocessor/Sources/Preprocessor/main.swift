import Foundation

let url = URL(fileURLWithPath: "/Users/zachwolfe/Documents/Swifter/Preprocessor/metadata.swiftermd")
do {
    let preprocessor = try Preprocessor(fileAt: url)
    try preprocessor.outputFiles(at: url.deletingLastPathComponent().deletingLastPathComponent().appendingPathComponent("Sources", isDirectory: true))
} catch {
    print(error)
}
